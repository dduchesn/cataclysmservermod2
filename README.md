The Cataclysm is coming...
Cataclysm offers a unique experience to players willing to commit to harsh conditions. We are a new server with plans for custom content.
We want to offer a PvPvE experience with long-term PvP goals. PvE is always welcome but the end-game goal is to create a PvP system on our server that makes the grind worth-while. We want to create a World that you want to play in so work with us and help us figure out the best gameplay experience for everyone!

http://cataclysm.duchmorri.com/

Special Thanks to our Patreons:
Loki
Sakai
Demon Goat
N3r0
Skordina
Nina
Katteru

Update - 04-22-2021
Well I think that time has come we all knew it was inevitable given our low player count it has time to come to an end. I am way to busy with life these days to continue maintaining the server and the mods. Given the newest update today is likely to have broken all the mods again, I will be providing the most recent backup for everyone who wishes to have a personal save files with the base they made on the server or want to see how things were made as an admin the server files are below as well as Cataclysm Server Mod 2 Source files feel free to use them as you wish.

to use the save file make a backup of your current save folder if you want to keep it in \steamapps\common\Conan Exiles\ConanSandbox and replace it with the cataclysm save folder

To use the source files for the mod you will need the dev kit & will have to create a new mod and put the files in the rar into that new mod source files folder.

It was fun all I appreciate all of you hanging around and playing with us i'm gonna keep the discord around and the server will eventually turn off. (its turned off atm due to most recent update and all mods still be outdated)


Update: 20 Mar @ 11:16pm
- items update for Big Goron Sword Quest
- changed weapon on Cataclysmic Champion

Update: 17 Mar @ 11:13pm
- Obsidian Cave Fixs
- map Updated to show Caves

Update: 16 Mar @ 2:33pm
- Quickfix

Update: 16 Mar @ 11:58am
- Fixed some issues from 2.3 update
- added new cave to the Ancient grove with many Bosses/Monsters

Update: 8 Mar @ 11:20pm
- Fine tuned Ancient Grove
- Fixed a couple texture issues in the data tables with Items
- Starter area overhaul
- Large new area in the south with good hunting for EEWA bounty and a some nice caves to build in.

Update: 28 Feb @ 11:31pm
- added many features/Monster spawns to the Ancient grove
- updated the map to be more accurate on placment of character for new area

Update: 25 Feb @ 11:36pm
- framework for Large Quest Chain Added
- Fixed Cataclysm Shield
- Ancient Grove Area Added ( can be access via the Ice caves)

Discuss this update in the discussions section.
- Updated mini map to include the new areas
- Fixed a couple holes in the ice caves
- Fixed a couple things around the Ruins
- New Items added for quest rewards

Update: 11 Feb @ 11:26pm
- Fixed hp on Cataclysmic Giants
- Added new farmable area & portal near the "Secret" garden in the Ruins
- Garden Gaurdian monster added
- Chong added to go along with Cheech NPC, Thanks Nina for the idea!
- Minor fixs around Ruins & Ice Caves
- Map Markers added when discovering a new location added to Cataclysm custom areas
- New weapon available from The Commander At Arm's outside of Funkman's Keep


Update: 10 Feb @ 11:27pm
- Expansion in Ice Caves now has Mobs
- Cataclysmic Giants added to cataclysm spawn table and seperate spawns in ruins/where cataclysm Warriors appear
- New weapons framework created
- New "special" garden added, can you find it ?
- 2 new custom item exchange NPC's


Copyright 2021 David Duchesneau-Hart

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
